/*
 * usart_rx.c
 *
 *  Created on: 10 de set de 2020
 *      Author: Lucas Muniz
 */
#include <stdio.h>
#include <string.h>
#include "lm_stm32f407xx.h"

void delay(void)
{
	for (uint32_t i = 0; i < 100000; i++);
}

void GPIO_ButtonInit(void)
{
	GPIO_Handle_t GPIOBtn;
	//this is btn gpio configuration
	GPIOBtn.pGPIOx = GPIOA;
	GPIOBtn.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_0;
	GPIOBtn.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_IN;
	GPIOBtn.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_FAST;
	GPIOBtn.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_NO_PUPD;
	GPIO_Init(&GPIOBtn);
}

char msg[1024] = "testing Tx Testing...\n\r";
//PA2 -> RX
//PA3 -> TX
void USART2_GPIOInit(void)
{
	GPIO_Handle_t USART_Pins;

	//GPIOA
	USART_Pins.pGPIOx = GPIOA;

	USART_Pins.GPIO_PinConfig.GPIO_PinMode =GPIO_MODE_ALTFN;
	USART_Pins.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	USART_Pins.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PU;
	USART_Pins.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_FAST;
	USART_Pins.GPIO_PinConfig.GPIO_PinAltFunMode = 7;

	// USART 2 TX -> PIN 2
	USART_Pins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_2;
	GPIO_Init(&USART_Pins);

	//USART 2 RX -> PIN 3
	USART_Pins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_3;
	GPIO_Init(&USART_Pins);
}

USART_Handle_t USART2_Handle;

void USART2_Init(void)
{
	USART2_Handle.pUSARTx = USART2;

	USART2_Handle.USART_Config.USART_Baud = USART_STD_BAUD_9600;
	USART2_Handle.USART_Config.USART_HWFlowControl = USART_HW_FLOW_CTRL_NONE;
	USART2_Handle.USART_Config.USART_Mode = USART_MODE_TXRX;
	USART2_Handle.USART_Config.USART_NoOfStopBits = USART_STOPBITS_1;
	USART2_Handle.USART_Config.USART_ParityControl = USART_PARITY_DISABLE;
	USART2_Handle.USART_Config.USART_WordLength = USART_WORDLEN_8BITS;

	USART_Init(&USART2_Handle);
}


int main(void)
{

	uint32_t Len;

	Len = strlen(msg);

	GPIO_ButtonInit();

	USART2_GPIOInit();

	USART2_Init();

	USART_PeripheralControl(USART2, ENABLE);

	while(1)
	{
		//wait for button is pressed
		while (!GPIO_ReadFromInputPin(GPIOA, GPIO_PIN_NO_0));

		//to avoid de-boucing
		delay();

		//send data
		USART_SendData(&USART2_Handle, (uint8_t*)msg, Len);
	}

	return 0;
}
