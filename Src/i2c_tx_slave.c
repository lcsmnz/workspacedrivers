/*
 * i2c_rxtx_slave.c
 *
 *  Created on: 16 de ago de 2020
 *      Author: Lucas Muniz
 */

//I2C test code with comunication with Arduino.

#include "lm_stm32f407xx.h"
#include <string.h>


#define SLAVE_ADDR 0X68
#define MY_ADDR SLAVE_ADDR

void delay(void)
{
	for (uint32_t i = 0; i < 100000; i++);
}

uint8_t Tx_buf[32] = "STM32 Slave Mode Testing...";

I2C_Handle_t I2CHandle;
/*
 * PB06 --> I2C1_SCL
 * PB07 --> I2C1_SDA
 * ALT function mode : 4
 */

void I2C1_GPIOInits(void)
{
	GPIO_Handle_t I2CPins;
	I2CPins.pGPIOx = GPIOB;
	I2CPins.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_ALTFN;
	I2CPins.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_OD;
	I2CPins.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PU;
	I2CPins.GPIO_PinConfig.GPIO_PinAltFunMode = 4;
	I2CPins.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_FAST;

	//SCL
	I2CPins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_6;
	GPIO_Init(&I2CPins);

	//SDA
	I2CPins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_7;
	GPIO_Init(&I2CPins);
}

void I2C1_Inits(void)
{
	I2CHandle.pI2Cx = I2C1;
	I2CHandle.I2C_config.I2C_ACKControl = I2C_ACK_ENABLE;
	I2CHandle.I2C_config.I2C_DeviceAddress = MY_ADDR;
	I2CHandle.I2C_config.I2C_FMDutyCycle = I2C_FM_DUTY_2;
	I2CHandle.I2C_config.I2C_SCLSpeed = I2C_SCL_SPEED_SM;

	I2C_Init(&I2CHandle);
}

void GPIO_ButtonInit(void)
{
	GPIO_Handle_t GPIOBtn;

	//this is btn gpio configuration
	GPIOBtn.pGPIOx = GPIOA;
	GPIOBtn.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_0;
	GPIOBtn.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_IN;
	GPIOBtn.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_FAST;
	GPIOBtn.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_NO_PUPD;

	GPIO_Init(&GPIOBtn);
}

// Application callback
void I2C_ApplicationEventCallback(I2C_Handle_t *pI2CHandle, uint8_t AppEv);

int main(void)
{

	//this function is used to initialize the GPIO pin as a button
	GPIO_ButtonInit();

	//this function is used to initialize the GPIO pins to behave as I2C1 pins
	I2C1_GPIOInits();

	//This function is used to initialize the I2C peripheral parameters
	I2C1_Inits();

	//I2C IRQ configuration
	I2C_IRQInterruptConfig(IRQ_NO_I2C1_EV, ENABLE);
	I2C_IRQInterruptConfig(IRQ_NO_I2C1_ER, ENABLE);

	I2C_SlaveEnableDisableCallBackEvents(I2C1, ENABLE);
	//This function is used to ENABLE the I2C1 peripheral
	I2C_PeripheralControl(I2C1, ENABLE);
	I2C_ManageAcking(I2C1, ENABLE);

	while (1)
	{

	}
	return 0;
}

void I2C1_EV_IRQHandler(void)
{
	I2C_EV_IRQHandling(&I2CHandle);
}

void I2C1_ER_IRQHandler(void)
{
	I2C_ER_IRQHandling(&I2CHandle);
}


void I2C_ApplicationEventCallback(I2C_Handle_t *pI2CHandle, uint8_t AppEv)
{
	static uint8_t CommandCode = 0;
	static uint8_t Cnt = 0;

	if (AppEv == I2C_EV_TX_CMPLT )
	{
		//Code for TX complete
	}
	else if (AppEv == I2C_EV_RX_CMPLT)
	{
		//Code for RX complete
	}
	else if (AppEv == I2C_EV_STOP)
	{
		//Code for Event STOP
		// Happens only on slave reception, Master has ended I2C Communication with Slave.
	}
	else if (AppEv == I2C_ERROR_BERR)
	{
		//Code for bus error
	}
	else if (AppEv == I2C_ERROR_ARLO)
	{
		// code for arbitration Lost detection
	}
	else if (AppEv == I2C_ERROR_AF)
	{
		// code for Acknowledge failure

		// in master it happens when slave fails to send ACK bit
		//I2C_CloseSendData(pI2CHandle);
		// generate stop condition to release the BUS
		//I2C_GenerateStopCondition(pI2CHandle->pI2Cx);

		//in Slave mode it happens when master send NAK -> Master doesn't  want more Dara
		CommandCode = 0xff;
		Cnt = 0;
	}
	else if (AppEv == I2C_ERROR_OVR)
	{
		// code for overrun error
	}
	else if (AppEv == I2C_ERROR_TIMEOUT)
	{
		// code for timeot error
	}
	else if (AppEv == I2C_EV_DATA_REQ)
	{
		// code for data REQUEST
		// Master wants DATA -> Slave has to send
		if(CommandCode == 0x51)
		{
			//send Length information to master
			I2C_SlaveSendData(pI2CHandle->pI2Cx, strlen((char*)Tx_buf));
		}
		else if(CommandCode == 0x52)
		{
			//send content of tx buffer
			I2C_SlaveSendData(pI2CHandle->pI2Cx, Tx_buf[Cnt++]);
		}
	}
	else if (AppEv == I2C_EV_DATA_RCV)
	{
		// Code for data RECEIVE
		// Data is waitng to the SLAVE to read.
		CommandCode = I2C_SlaveReceiveData(pI2CHandle->pI2Cx);

	}
}
