/*
 * spi_txonly_arduino.c
 *
 *  Created on: Jun 28, 2020
 *      Author: Lucas Muniz
 */

#include <string.h>
#include "lm_stm32f407xx.h"

I2C_Handle_t I2CHandle;
#define MY_ADDR 0x61
#define SLAVE_ADDR 0x68
//char user_data[] = "OLA, LUCAS(EU)";
uint8_t data[14] = {0x4F, 0x4C, 0x41, 0x2C, 0x20, 0x4C, 0x55, 0x43, 0x41, 0x53, 0x28, 045, 0x55, 0x29};
uint32_t size_data = 14;

void delay(void)
{
	for (uint32_t i = 0; i < 100000; i++)
		;
}

/*
 * PB06 --> I2C1_SCL
 * PB07 --> I2C1_SDA
 * ALT function mode : 4
 */

void I2C1_GPIOInits(void)
{
	GPIO_Handle_t I2CPins;
	I2CPins.pGPIOx = GPIOB;
	I2CPins.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_ALTFN;
	I2CPins.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_OD;
	I2CPins.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PU;
	I2CPins.GPIO_PinConfig.GPIO_PinAltFunMode = 4;
	I2CPins.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_FAST;

	//SCL
	I2CPins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_6;
	GPIO_Init(&I2CPins);

	//SDA
	I2CPins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_7;
	GPIO_Init(&I2CPins);
}

void I2C1_Inits(void)
{
	I2CHandle.pI2Cx = I2C1;
	I2CHandle.I2C_config.I2C_ACKControl = I2C_ACK_ENABLE;
	I2CHandle.I2C_config.I2C_DeviceAddress = MY_ADDR;
	I2CHandle.I2C_config.I2C_FMDutyCycle = I2C_FM_DUTY_2;
	I2CHandle.I2C_config.I2C_SCLSpeed = I2C_SCL_SPEED_SM;

	I2C_Init(&I2CHandle);
}

void GPIO_ButtonInit(void)
{
	GPIO_Handle_t GPIOBtn;

	//this is btn gpio configuration
	GPIOBtn.pGPIOx = GPIOA;
	GPIOBtn.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_0;
	GPIOBtn.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_IN;
	GPIOBtn.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_FAST;
	GPIOBtn.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_NO_PUPD;

	GPIO_Init(&GPIOBtn);
}

int main(void)
{

	GPIO_ButtonInit();

	//this function is used to initialize the GPIO pins to behave as I2C1 pins
	I2C1_GPIOInits();

	//This function is used to initialize the I2C peripheral parameters
	I2C1_Inits();

	//This function is used to ENABLE the I2C1 peripheral
	I2C_PeripheralControl(I2C1, ENABLE);

	while (1)
	{
		//wait till button is pressed
		while (!GPIO_ReadFromInputPin(GPIOA, GPIO_PIN_NO_0))
			;
		delay();
		//send data
		I2C_MasterSendData(&I2CHandle, data, size_data, SLAVE_ADDR, I2C_NO_STOP_CONDITION);
	}

	return 0;
}
