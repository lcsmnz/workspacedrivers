/*
 * i2d_txrx_it.c
 *
 *  Created on: 29 de jul de 2020
 *      Author: Lucas Muniz
 */

//I2C test code with comunication with BMP280 sensor.

#include "lm_stm32f407xx.h"

I2C_Handle_t I2CHandle;
#define MY_ADDR 0x61
#define BMP_ADDR 0x76
#define BMP_ID_REG 0xD0
#define BMP_TEMP_REG 0xFA
#define BMP_PRES_REG 0xF7
//char user_data[] = "OLA, LUCAS(EU)";

uint32_t size_data = 14;

void delay(void)
{
	for (uint32_t i = 0; i < 100000; i++)
		;
}

/*
 * PB06 --> I2C1_SCL
 * PB07 --> I2C1_SDA
 * ALT function mode : 4
 */

void I2C1_GPIOInits(void)
{
	GPIO_Handle_t I2CPins;
	I2CPins.pGPIOx = GPIOB;
	I2CPins.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_ALTFN;
	I2CPins.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_OD;
	I2CPins.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PU;
	I2CPins.GPIO_PinConfig.GPIO_PinAltFunMode = 4;
	I2CPins.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_FAST;

	//SCL
	I2CPins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_6;
	GPIO_Init(&I2CPins);

	//SDA
	I2CPins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_7;
	GPIO_Init(&I2CPins);
}

void I2C1_Inits(void)
{
	I2CHandle.pI2Cx = I2C1;
	I2CHandle.I2C_config.I2C_ACKControl = I2C_ACK_ENABLE;
	I2CHandle.I2C_config.I2C_DeviceAddress = MY_ADDR;
	I2CHandle.I2C_config.I2C_FMDutyCycle = I2C_FM_DUTY_2;
	I2CHandle.I2C_config.I2C_SCLSpeed = I2C_SCL_SPEED_SM;

	I2C_Init(&I2CHandle);
}

void GPIO_ButtonInit(void)
{
	GPIO_Handle_t GPIOBtn;

	//this is btn gpio configuration
	GPIOBtn.pGPIOx = GPIOA;
	GPIOBtn.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_0;
	GPIOBtn.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_IN;
	GPIOBtn.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_FAST;
	GPIOBtn.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_NO_PUPD;

	GPIO_Init(&GPIOBtn);
}

int main(void)
{
	uint8_t BMP_RegAddr = BMP_ID_REG, BMP_Addr = BMP_ADDR;
	//this function is used to initaalize de GPIO pin as a button
	GPIO_ButtonInit();

	//this function is used to initialize the GPIO pins to behave as I2C1 pins
	I2C1_GPIOInits();

	//This function is used to initialize the I2C peripheral parameters
	I2C1_Inits();

	//I2C IRQ configuration
	I2C_IRQInterruptConfig(IRQ_NO_I2C1_EV, ENABLE);
	I2C_IRQInterruptConfig(IRQ_NO_I2C1_ER, ENABLE);

	//This function is used to ENABLE the I2C1 peripheral
	I2C_PeripheralControl(I2C1, ENABLE);
	I2C_ManageAcking(I2C1, ENABLE);
	uint8_t DataBMP = 0;

	while (1)
	{
		//wait till button is pressed
		while (!GPIO_ReadFromInputPin(GPIOA, GPIO_PIN_NO_0));
		delay();
		//Send Slave id REG address
		while(I2C_MasterSendDataIT(&I2CHandle, &BMP_RegAddr, 1, BMP_Addr, I2C_STOP_CONDITION) != I2C_READY);

		while(I2C_MasterReceiveDataIT(&I2CHandle, &DataBMP, 1, BMP_Addr, I2C_STOP_CONDITION) != I2C_READY);

	}

	return 0;
}


void I2C1_EV_IRQHandler(void)
{
	I2C_EV_IRQHandling(&I2CHandle);
}

void I2C1_ER_IRQHandler(void)
{
	I2C_ER_IRQHandling(&I2CHandle);

}


