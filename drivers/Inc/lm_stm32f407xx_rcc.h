/*
 * stm32f407xx_rcc_driver.h
 *
 *  Created on: Jun 28, 2020
 *      Author: Lucas Muniz
 */

#ifndef INC_LM_STM32F407XX_RCC_H_
#define INC_LM_STM32F407XX_RCC_H_

#include "lm_stm32f407xx.h"

//This returns the APB1 clock value
uint32_t RCC_GetPCLK1Value(void);

//This returns the APB2 clock value
uint32_t RCC_GetPCLK2Value(void);

uint32_t RCC_GetPLLOutputClock(void);
#endif /* INC_LM_STM32F407XX_RCC_H_ */
