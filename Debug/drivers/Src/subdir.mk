################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../drivers/Src/lm_stm32f407xx_gpio.c \
../drivers/Src/lm_stm32f407xx_i2c.c \
../drivers/Src/lm_stm32f407xx_rcc.c \
../drivers/Src/lm_stm32f407xx_spi.c 

OBJS += \
./drivers/Src/lm_stm32f407xx_gpio.o \
./drivers/Src/lm_stm32f407xx_i2c.o \
./drivers/Src/lm_stm32f407xx_rcc.o \
./drivers/Src/lm_stm32f407xx_spi.o 

C_DEPS += \
./drivers/Src/lm_stm32f407xx_gpio.d \
./drivers/Src/lm_stm32f407xx_i2c.d \
./drivers/Src/lm_stm32f407xx_rcc.d \
./drivers/Src/lm_stm32f407xx_spi.d 


# Each subdirectory must supply rules for building sources it contributes
drivers/Src/lm_stm32f407xx_gpio.o: ../drivers/Src/lm_stm32f407xx_gpio.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F407G_DISC1 -DSTM32F4 -DSTM32F407VGTx -DDEBUG -c -I../Inc -I"D:/WorkspaceDrivers/lm_stm32f407xx_drivers/drivers/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/lm_stm32f407xx_gpio.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/lm_stm32f407xx_i2c.o: ../drivers/Src/lm_stm32f407xx_i2c.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F407G_DISC1 -DSTM32F4 -DSTM32F407VGTx -DDEBUG -c -I../Inc -I"D:/WorkspaceDrivers/lm_stm32f407xx_drivers/drivers/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/lm_stm32f407xx_i2c.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/lm_stm32f407xx_rcc.o: ../drivers/Src/lm_stm32f407xx_rcc.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F407G_DISC1 -DSTM32F4 -DSTM32F407VGTx -DDEBUG -c -I../Inc -I"D:/WorkspaceDrivers/lm_stm32f407xx_drivers/drivers/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/lm_stm32f407xx_rcc.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/lm_stm32f407xx_spi.o: ../drivers/Src/lm_stm32f407xx_spi.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F407G_DISC1 -DSTM32F4 -DSTM32F407VGTx -DDEBUG -c -I../Inc -I"D:/WorkspaceDrivers/lm_stm32f407xx_drivers/drivers/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/lm_stm32f407xx_spi.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

